import subprocess
import re
import sys

dictionary = []

dictPath = sys.argv[1]
currentDir = sys.argv[2]
outFile = open(sys.argv[3], "w")
outTestFile = open(sys.argv[4], "w")
# proc = subprocess.Popen(["find", currentDir, "|", "grep", "ham.txt"]
#                         , stdout=subprocess.PIPE, shell=True)
proc = subprocess.Popen("find " + currentDir + " | grep ham.txt",
                        stdout=subprocess.PIPE, shell=True)
(out, err) = proc.communicate()
out = out.decode("latin1")
hamFiles = out.split("\n")
hamFiles = filter(lambda a: a != "", hamFiles)

# proc = subprocess.Popen(["find", currentDir, "|", "grep", "txt", "|", "grep", "spam"]
                        # , stdout=subprocess.PIPE, shell=True)
# proc = subprocess.Popen(["find", currentDir, "|", "grep", "spam.txt"]
#                         , stdout=subprocess.PIPE, shell=True)
proc = subprocess.Popen("find " + currentDir + " | grep spam.txt",
                        stdout=subprocess.PIPE, shell=True)
(out, err) = proc.communicate()
out = out.decode("latin1")
spamFiles = out.split('\n')
spamFiles = filter(lambda a: a != "", spamFiles)

# proc = subprocess.Popen(["find", currentDir, "|", "grep", "spam_or_ham_test", "|", "grep", "txt"]
#                         , stdout=subprocess.PIPE, shell=True)
proc = subprocess.Popen("find " + currentDir + " | grep spam_or_ham_test | grep txt",
                        stdout=subprocess.PIPE, shell=True)
(out, err) = proc.communicate()
out = out.decode("latin1")
spamTestFiles = out.split('\n')
spamTestFiles = filter(lambda a: a != "", spamTestFiles)

dictionary = {}

def compactReview(fileName, category):
    with open(fileName, encoding="latin1") as inFile:
        reviewStat = {}
        outLine = ""
        review = inFile.read()
        tokens = re.split("\\s+", review)
        # print tokens
        for token in tokens:
            if token in dictionary:
                index = dictionary.get(token)
                if index in reviewStat:
                    reviewStat.update({index: reviewStat.get(index) + 1})
                else:
                    reviewStat.update({index: 1})
        for key, value in sorted(reviewStat.items()):
            outLine += str(key) + ":" + str(value) + " "
        if "ham" in category:
            return "HAM " + outLine # ham
        return "SPAM " + outLine # spam

# read the dictionary
with open(dictPath, encoding="latin1") as dictFile:
    dictList = dictFile.readlines()
dictList = [a.strip() for a in dictList]
dictList = filter(lambda a: a.strip() != "", dictList)
i = 0
for item in dictList:
    dictionary.update({item: i})
    i += 1

# produce the feature files
for fileName in hamFiles:
    outFile.write(compactReview(fileName, "ham") + "\n")
for fileName in spamFiles:
    outFile.write(compactReview(fileName, "spam") + "\n")
for fileName in spamTestFiles:
    outTestFile.write(compactReview(fileName, "spam") + "\n")
