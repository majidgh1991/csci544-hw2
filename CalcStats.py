import sys
import re

refFile = open(sys.argv[1])
testFile = open(sys.argv[2])
problem = sys.argv[3]

classNames = ["", ""]
falsePositives = [0, 0]
falseNegatives = [0, 0]
totalsTest = [0, 0]
totalsRef = [0, 0]
if "spam" in problem:
    classNames[0] = "SPAM"
    classNames[1] = "HAM"
else:
    classNames[0] = "POSITIVE"
    classNames[1] = "NEGATIVE"
for (refLine, testLine) in zip(refFile, testFile):
    if refLine != testLine:
        if classNames[0] in refLine: # false positive for class 1, false negative for class 0
            falsePositives[1] += 1
            falseNegatives[0] += 1
            totalsTest[1] += 1
            totalsRef[0] += 1
        else: #classNames[1] in refLine:
            falsePositives[0] += 1
            falseNegatives[1] += 1
            totalsTest[0] += 1
            totalsRef[1] += 1
    else:
        if classNames[0] in testLine:
            totalsTest[0] += 1
            totalsRef[0] += 1
        else:
            totalsTest[1] += 1
            totalsRef[1] += 1
recall = [0, 0]
precision = [0, 0]
fscore = [0, 0]
print(totalsTest[0])
print(totalsTest[1])
recall[0] = float(totalsTest[0] - falsePositives[0])/float(totalsRef[0])
recall[1] = float(totalsTest[1] - falsePositives[1])/float(totalsRef[1])
precision[0] = float(totalsTest[0] - falsePositives[0])/float(totalsTest[0])
precision[1] = float(totalsTest[1] - falsePositives[1])/float(totalsTest[1])
fscore[0] = 2*precision[0]*recall[0]/(precision[0] + recall[0])
fscore[1] = 2*precision[1]*recall[1]/(precision[1] + recall[1])
print(classNames[0] + ": (" + str(precision[0]) + ", " + str(recall[0]) + ", " + str(recall[0]) + ")")
print(classNames[1] + ": (" + str(precision[1]) + ", " + str(recall[1]) + ", " + str(recall[1]) + ")")