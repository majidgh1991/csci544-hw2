import sys
import ast
import re
import math

classes = [{}, {}]
weights = []
problem = sys.argv[3]
with open(sys.argv[2]) as model_file:
    classes[0] = eval(model_file.readline().strip())
    classes[1] = eval(model_file.readline().strip())
    # for line in model_file:
    #     temp = ast.literal_eval(line.strip())
    #     if line.strip() != "":
    #         print(temp)
    #         classes.append(temp)
predictedClass = 0
correct = 0
wrong = 0
cl = 0
with open(sys.argv[1]) as input_file:
    for line in input_file:
        tokens = re.split("\\s+", line)
        weights = [0, 0]
        for token in tokens:
            if token is "":
                continue
            if ":" in token: #it's feature
                subtokens = token.split(":")
                feature = int(subtokens[0])
                count = int(subtokens[1])
                if feature in classes[0]:
                    weights[0] += float(count)*math.log(classes[0].get(feature))
                if feature in classes[1]:
                    weights[1] += float(count)*math.log(classes[1].get(feature))
            else: # label
                if "ham" in problem:
                    if "HAM" in token:
                        cl = 1
                    else:
                        cl = 0
                else:
                    if int(token) >= 7:
                        cl = 0
                    else:
                        cl = 1
        if weights[0] > weights[1]:
            # print(str(weights[0]) + " " + str(weights[1]))
            predictedClass = 0
        else:
            # print(str(weights[0]) + " " + str(weights[1]))
            predictedClass = 1
        if predictedClass == cl:
            correct += 1
        else:
            wrong += 1
        if "ham" in problem:
            if predictedClass == 1:
                print("HAM")
            else:
                print("SPAM")
        else:
            if predictedClass == 1:
                print("NEGATIVE")
            else:
                print("POSITIVE")
