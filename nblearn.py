import sys
import re

classes = [{}, {}]
totalCount = [0, 0]
problem = sys.argv[4] # hamspam / sentiment
# spam: class0, ham: class1
# positive: class0, negative: class1
dictSize = 0
dictFilePath = sys.argv[3]
with open(dictFilePath, encoding="latin1") as dictFile:
    lines = [line for line in dictFile]
    dictSize = len(lines)
outFile = open(sys.argv[2], "w")
with open(sys.argv[1]) as input_file:
    for line in input_file:
        tokens = re.split("\\s+", line)
        cl = -1
        for token in tokens:
            if token == "":
                continue
            if ":" in token: # feature
                subtokens = token.split(":")
                feature = int(subtokens[0])
                count = int(subtokens[1])
                totalCount[cl] += count
                if feature in classes[cl]:
                    classes[cl].update({feature: classes[cl].get(feature)+count})
                else:
                    classes[cl].update({feature: count})
            else: # label
                if "ham" in problem:
                    if "HAM" in token:
                        cl = 1
                    else:
                        cl = 0
                else:
                    if int(token) >= 7:
                        cl = 0
                    else:
                        cl = 1

#convert counts to probabilities
count = 0
toberemoved = []
vals = [[], []]
for key, value in classes[0].items():
    vals[0].append(value)

for key, value in classes[1].items():
    vals[1].append(value)

vals[0].sort()
vals[1].sort()
X = 20000
for key, value in classes[0].items():
    if key in classes[1]:
        if classes[1].get(key) > vals[1][int(len(vals[1]) - len(vals[1])/X) - 1] and \
                        classes[0].get(key) > vals[0][int(len(vals[0]) - len(vals[0])/X) - 1]:
                        # the term is repeated in both classes and not that distinctive
            toberemoved.append(key)
    classes[0].update({key: float(value + 1)/float(totalCount[0] + dictSize)})

for key in toberemoved:
    del classes[1][key]
    del classes[0][key]
    count += 1

for key, value in classes[1].items():
    classes[1].update({key: (float(value + 1)/float(totalCount[1] + dictSize))})

outFile.write(str(classes[0]) + "\n" + str(classes[1]))
print(count)