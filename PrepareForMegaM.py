import re
import sys

out_file = open(sys.argv[2], "w")
dataset = sys.argv[3]
with open(sys.argv[1]) as input_file:
    for line in input_file:
        tokens = re.split("\\s+", line)
        new_line = ""
        for token in tokens:
            if token is "":
                continue
            if ":" in token: #it's feature
                subtokens = token.split(":")
                feature = subtokens[0]
                count = subtokens[1]
                new_line += str(int(feature)+1) + ":" + count + " "
            else: #it's label
                if "test" in dataset:
                    continue
                if "ham" in dataset:
                    if "HAM" in token:
                        new_line += "0 "
                    else:
                        new_line += "1 "
                else:
                    if int(token) >= 7:
                        new_line += "1 "
                    else:
                        new_line += "0 "
        if "test" in sys.argv[3]:
            new_line = "0 " + new_line
        out_file.write(new_line+"\n")
