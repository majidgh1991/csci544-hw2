import random
import sys

trainingFile = open(sys.argv[3], "w")
testFile = open(sys.argv[4], "w")
with open(sys.argv[1]) as input_file:
    lines = input_file.readlines()
random.shuffle(lines)
trainingPortion = float(sys.argv[2])
index = int(trainingPortion * len(lines))
trainingFile.write("".join(lines[0:index]))
testFile.write("".join(lines[index+1:len(lines)-1]))
# print lines